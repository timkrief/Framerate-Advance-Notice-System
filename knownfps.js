//		Copyright (C) 2021 Tim Krief
//
//	This file is part of the Framerate Advance Notice System web extension.
//
//	The Framerate Advance Notice System web extension
//	is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	The Framerate Advance Notice System web extension
//	is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with the Framerate Advance Notice System web extension.
//	If not, see <https://www.gnu.org/licenses/>.

const observer = new MutationObserver(reactToNewVideos);
const maxParents = 3;
let textSize = "100%";

function displayFramerateHint(video){
	if(!video.dataset.addonFans && video.querySelector(".addon_FANS_framerateList") == null){
		console.log("addon FANS: adding framerate hint to video " + "https://www.youtube.com" + video.getAttribute("href"));
		
		xhr = new XMLHttpRequest();
		xhr.open('GET', "https://www.youtube.com" + video.getAttribute("href"));
		xhr.onreadystatechange = function() {
			if(xhr.readyState === XMLHttpRequest.DONE){
				var status = xhr.status;
				if(status === 0 || (status >= 200 && status < 400)){
					video.dataset.addonFans = true;
					console.log("addon FANS: request succeeded");
					let framerateMatches = xhr.responseText.matchAll(/\"fps\":([0-9]+)/gi);
					let framerates = [];
					for(let framerateMatch of framerateMatches){
						framerate = parseInt(framerateMatch[1]);
						if(framerates.indexOf(framerate) < 0){
							framerates.push(framerate);
						}
						console.log("addon FANS: framerate found: " + framerate)
					}
					const list = document.createElement("ul");
					list.className = "addon_FANS_framerateList ytd-thumbnail-overlay-time-status-renderer";
					for(const framerate of framerates){
						console.log(framerate);
						const listElement = document.createElement("li");
						const text = document.createTextNode(framerate.toString());
						listElement.appendChild(text);
						list.appendChild(listElement);
					}
					
					video.appendChild(list);
					
				}
			}
		}
		
		xhr.send();
	}
}

function reactToNewVideos(mutationList, observer) {
	console.log("addon FANS: page changed");
	mutationList.forEach( function(mutation) {
		for(const node of mutation.addedNodes){
			let cursor = node;
			let videoFound = false;
			let parents = 0;
			
			while(parents <= maxParents && cursor != null && cursor.tagName != null){
				if(cursor.tagName.toLowerCase() == "ytd-thumbnail"){
					videoFound = true;
					break
				}
				cursor = cursor.parentElement;
				parents += 1;
			}
			if(videoFound){
				console.log("addon FANS: new video added");
				cursor.addEventListener("mouseenter", function(){ displayFramerateHint(cursor.querySelector("a#thumbnail")) } );
				break;
			}
		}
	});
}

observer.observe(document, {childList: true, subtree: true});

function onError(error) {
	console.log(`addon FANS error: ${error}`);
}

function onGot(item) {
	let element = document.createElement('style');

	document.head.appendChild(element);

	let styleSheet = element.sheet;
	
	let littleSizeRule = 'a ul.addon_FANS_framerateList { font-size: ' + item.littleSize + ';}';
	let bigSizeRule = '*[width="9999"]>a ul.addon_FANS_framerateList { font-size: ' + item.bigSize + ';}';
	styleSheet.insertRule(littleSizeRule, 0);
	styleSheet.insertRule(bigSizeRule, 1);
}

let getting = browser.storage.sync.get(["littleSize", "bigSize"]);
getting.then(onGot, onError);

