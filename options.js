//		Copyright (C) 2021 Tim Krief
//
//	This file is part of the Framerate Advance Notice System web extension.
//
//	The Framerate Advance Notice System web extension
//	is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	The Framerate Advance Notice System web extension
//	is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with the Framerate Advance Notice System web extension.
//	If not, see <https://www.gnu.org/licenses/>.

const cssFontsizeRegex = /^\s*(([+-]?\d*\.?\d+[\s]*(?:cap|ch|em|ex|ic|lh|rem|rlh|vh|vw|vi|vb|vmin|vmax|px|cm|mm|Q|in|pc|pt|%))|(?:larger|smaller)|(?:xx-small|x-small|small|medium|large|x-large|xx-large|xxx-large))\s*$/

document.addEventListener("DOMContentLoaded", function() {
	browser.storage.sync.get(["littleSize", "bigSize"]).then(
	function(storage) {
		document.querySelector("#littleSize").value = storage.littleSize || "1.2rem";
		document.querySelector("#bigSize").value = storage.bigSize || "2.5rem";
	},
	function(error) {
		console.log("addon FANS error:" + error);
	});
});

document.querySelector("form").addEventListener("submit", function(event) {
	event.preventDefault();
	
	let littleSize = "1.2rem";
	if(cssFontsizeRegex.test(document.querySelector("#littleSize").value)){
		littleSize = document.querySelector("#littleSize").value;
	}
	
	let bigSize = "2.5rem";
	if(cssFontsizeRegex.test(document.querySelector("#bigSize").value)){
		bigSize = document.querySelector("#bigSize").value;
	}
	
	browser.storage.sync.set({
		littleSize: littleSize,
		bigSize: bigSize,
	});
});

