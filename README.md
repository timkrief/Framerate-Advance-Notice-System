# F.A.N.S. Framerate Advance Notice System

The Framerate Advance Notice System lets you know the available framerate options on a Youtube video by hovering its thumbnail.

How it works: when you install it and refresh your tabs pointing to a youtube.com page, when hovering a thumbnail, the browser will actually dynamically load the raw html source from the video page to get the framerate information.

The Framerate Advance Notice System web extension is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

All product and company names are trademarks™ or registered® trademarks of their respective holders. Use of them does not imply any affiliation with or endorsement by them.

Donate: https://www.paypal.com/donate/?hosted_button_id=4NUJG3D667BU6
